#ifndef DIALOGSTATION_H
#define DIALOGSTATION_H

#include <QDialog>
#include <QList>
#include "Station.h"

namespace Ui {
class DialogStation;
}

class DialogStation : public QDialog
{
    Q_OBJECT

public:
    explicit DialogStation(QList<Station> &station, QWidget *parent=0);
    ~DialogStation();

    int getRes() const;
    void setRes(int value);

private slots:
    void on_comboBox_currentIndexChanged(int index);

private:
    Ui::DialogStation *ui;
    int res;
};

#endif // DIALOGSTATION_H
