#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "Map.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_actionNew_triggered();

    void on_actionOpen_triggered();

    void on_actionAll_stations_triggered();

    void on_actionAll_transport_triggered();

    void on_actionAll_transport_on_station_triggered();

    void on_actionRoute_of_the_transport_triggered();

    void on_toolAdd_clicked();

    void on_toolDelete_clicked();

    void on_toolMoveUp_clicked();

    void on_toolMoveDown_clicked();

private:
    Map map;
    enum Status {UNDEFINED, ALLSTATIONS, ALLTRANSPORT, TRANSPORTONSTATION, ROUTE}status;
    Ui::MainWindow *ui;

};

#endif // MAINWINDOW_H
