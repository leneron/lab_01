#-------------------------------------------------
#
# Project created by QtCreator 2015-03-08T15:00:01
#
#-------------------------------------------------

QT       += core sql

QT       += gui

QT       += widgets

TARGET = lab_01
CONFIG   -= app_bundle
CONFIG -= console

TEMPLATE = app


SOURCES += main.cpp \
    Map.cpp \
    Station.cpp \
    Transport.cpp \
    mainwindow.cpp \
    dialogroute.cpp \
    dialogaddstation.cpp \
    dialogaddtransport.cpp \
    dialogstation.cpp

HEADERS += \
    Transport.h \
    Station.h \
    Map.h \
    mainwindow.h \
    dialogroute.h \
    dialogaddstation.h \
    dialogaddtransport.h \
    dialogstation.h

FORMS += \
    mainwindow.ui \
    dialogroute.ui \
    dialogaddstation.ui \
    dialogaddtransport.ui \
    dialogstation.ui

RESOURCES += \
    icons.qrc

