#include "dialogroute.h"
#include "ui_dialogroute.h"
#include "Transport.h"

DialogRoute::DialogRoute(QList <Transport> &transport, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogRoute)
{
    ui->setupUi(this);
    for (int i = 0; i < transport.size(); i++)
        ui->comboBox->addItem(transport[i].getName());
}

DialogRoute::~DialogRoute()
{
    delete ui;
}
int DialogRoute::getRes() const
{
    return res;
}

void DialogRoute::setRes(int value)
{
    res = value;
}


void DialogRoute::on_comboBox_currentIndexChanged(int index)
{
    setRes(index);
}
