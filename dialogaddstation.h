#ifndef DIALOGADDSTATION_H
#define DIALOGADDSTATION_H

#include <QDialog>
#include <QString>

namespace Ui {
class DialogAddStation;
}

class DialogAddStation : public QDialog
{
    Q_OBJECT

public:
    explicit DialogAddStation(QWidget *parent = 0);
    ~DialogAddStation();

    QString getRes() const;
    void setRes(const QString &value);

private slots:
    void on_lineEdit_editingFinished();

private:
    Ui::DialogAddStation *ui;
    QString res;
};

#endif // DIALOGADDSTATION_H
