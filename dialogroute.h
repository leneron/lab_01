#ifndef DIALOGROUTE_H
#define DIALOGROUTE_H

#include <QDialog>
#include "Transport.h"

namespace Ui {
class DialogRoute;
}

class DialogRoute : public QDialog
{
    Q_OBJECT

public:
    explicit DialogRoute(QList<Transport> &transport, QWidget *parent = 0);
    ~DialogRoute();

    int getRes() const;
    void setRes(int value);

private slots:
    void on_comboBox_currentIndexChanged(int index);

private:
    Ui::DialogRoute *ui;
    int res;
};

#endif // DIALOGROUTE_H
