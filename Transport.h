#ifndef TRANSPORT_H
#define TRANSPORT_H
#include <QString>

class Transport
{
public:

    Transport(const QString& valueKind, const QString& valueName, float interval);
    Transport(const QString& valueKind, const QString& valueName,
              float valueInterval, int valueId);
    ~Transport();
    QString getKind() const;
    void setKind(const QString &value);

    QString getName() const;
    void setName(const QString &value);

    float getInterval() const;
    void setInterval(float value);

    int getId() const;
    void setId(int value);


private:
    enum Kind{BUS, TROLLEYBUS, TRAM, UNDEFINED} kind;
    QString name;
    int id;
    float interval;
};

#endif // TRANSPORT_H
