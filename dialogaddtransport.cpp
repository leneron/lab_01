#include "dialogaddtransport.h"
#include "ui_dialogaddtransport.h"
#include <QString>

DialogAddTransport::DialogAddTransport(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogAddTransport)
{
    ui->setupUi(this);
    ui->comboBox->addItem("BUS");
    ui->comboBox->addItem("TROLLEYBUS");
    ui->comboBox->addItem("TRAM");
}

DialogAddTransport::~DialogAddTransport()
{
    delete ui;
}
QString DialogAddTransport::getKind() const
{
    return kind;
}

void DialogAddTransport::setKind(const QString &value)
{
    kind = value;
}
QString DialogAddTransport::getName() const
{
    return name;
}

void DialogAddTransport::setName(const QString &value)
{
    name = value;
}
float DialogAddTransport::getInterval() const
{
    return interval;
}

void DialogAddTransport::setInterval(float value)
{
    interval = value;
}


void DialogAddTransport::on_lineEdit_editingFinished()
{
    setName(ui->lineEdit->text());
}

void DialogAddTransport::on_lineEdit_2_editingFinished()
{
    QString intervalValue = ui->lineEdit_2->text();
    setInterval(intervalValue.toFloat());
}

void DialogAddTransport::on_comboBox_currentIndexChanged(const QString &arg1)
{
    setKind(arg1);
}
