#include <QCoreApplication>
#include "Map.h"
#include "Station.h"
#include "Transport.h"
#include <QDebug>
#include <QApplication>
#include "mainwindow.h"

int main(int argc, char *argv[])
{

    QApplication app(argc, argv);
    MainWindow window;

    window.show();
    window.setWindowTitle("Transport routes");

    return app.exec();
}
