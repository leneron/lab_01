#include "Transport.h"
#include <QDebug>

Transport::Transport(const QString &valueKind, const QString &valueName, float valueInterval)
{
    setKind(valueKind);

    name = valueName;
    interval = valueInterval;
}

Transport::Transport(const QString &valueKind, const QString &valueName, float valueInterval, int valueId)
{
    setKind(valueKind);

    name = valueName;
    interval = valueInterval;
    id = valueId;
}

Transport::~Transport()
{

}

QString Transport::getKind() const
{
    switch(kind)
    {
    case 0: return "BUS";
    case 1: return "TROLLEYBUS";
    case 2: return "TRAM";
    case 3: return "UNDEFINED";
    }
}

void Transport::setKind(const QString &value)
{
    if (value == "BUS") kind = BUS;
    else if (value == "TROLLEYBUS") kind = TROLLEYBUS;
    else if (value == "TRAM") kind = TRAM;
    else kind = UNDEFINED;
}

float Transport::getInterval() const
{
    return interval;
}

void Transport::setInterval(float value)
{
    interval = value;
}

QString Transport::getName() const
{
    return name;
}

void Transport::setName(const QString &value)
{
    name = value;
}

int Transport::getId() const
{
    return id;
}

void Transport::setId(int value)
{
    id = value;
}
