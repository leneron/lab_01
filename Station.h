#ifndef STATION_H
#define STATION_H
#include <QString>
#include <QPoint>

class Station
{
public:

    Station(const QString &valueName);
    Station(const QString &valueName, int valueId);//, const QPoint &valueP);
    Station(const QString &valueName, int valueId, int valueNumero);
    ~Station();

    QString getName() const;
    void setName(const QString &value);

    int getId() const;
    void setId(int value);

   // QPoint getP() const;
   // void setP(const QPoint &value);

private:
    QString name;
    int id;
   // QPoint p;
};

#endif // STATION_H
