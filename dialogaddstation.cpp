#include "dialogaddstation.h"
#include "ui_dialogaddstation.h"

#include <QString>

DialogAddStation::DialogAddStation(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogAddStation)
{
    ui->setupUi(this);
}

DialogAddStation::~DialogAddStation()
{
    delete ui;
}
QString DialogAddStation::getRes() const
{
    return res;
}

void DialogAddStation::setRes(const QString &value)
{
    res = value;
}


void DialogAddStation::on_lineEdit_editingFinished()
{
    setRes(ui->lineEdit->text());
}
