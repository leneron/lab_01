#include "Map.h"
#include <QtSql>
#include <QString>
#include <QList>
#include <iostream>
#include <QDebug>

Map::Map(const QString &filename)
{
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE", "SQLMap");
    db.setDatabaseName(filename);
    bool ok = db.open();
   // qDebug() << ok;
}

Map::Map()
{
}


Map::~Map()
{
    //db.close();
}

void Map::createNewTables()
{
    QSqlDatabase db = QSqlDatabase::database("SQLMap"); //db.open();
    QSqlQuery query(db);
    query.exec("CREATE TABLE routeTable ("
               "idTransport INTEGER, "
               "idStation INTEGER, "
               "numero INTEGER DEFAULT 0, "
               "PRIMARY KEY(idTransport,idStation,numero))");
    query.exec("CREATE TABLE stationTable ("
               "id INTEGER PRIMARY KEY AUTOINCREMENT, "
               "name TEXT)");
    query.exec("CREATE TABLE transportTable ("
               "id INTEGER PRIMARY KEY AUTOINCREMENT, "
               "name TEXT, "
               "interval REAL, "
               "kind TEXT)");
    //db.close();
}

void Map::addStation(Station &station)
{
    QSqlDatabase db = QSqlDatabase::database("SQLMap"); //db.open();
    QSqlQuery query(db);
    query.prepare("INSERT INTO stationTable (name) "
                  "VALUES(:name)");
    query.bindValue(":name", station.getName());
    query.exec();

   /* if (!query.exec())
    {
        qDebug() << query.lastError().text();
    }*/

    station.setId(query.lastInsertId().toInt());
    allStations.push_back(station);
    //db.close();
}

void Map::addTransport(Transport &transport)
{
    QSqlDatabase db = QSqlDatabase::database("SQLMap"); //db.open();
    QSqlQuery query(db);
    query.prepare("INSERT INTO transportTable (kind, name, interval) "
                  "VALUES(:kind, :name, :interval)");
    query.bindValue(":kind", transport.getKind());
    query.bindValue(":name", transport.getName());
    query.bindValue(":interval", transport.getInterval());
    query.exec();

    transport.setId(query.lastInsertId().toInt());
  //  qDebug() << transport.getKind() << " " << transport.getName() << " " << transport.getInterval() << " " << transport.getId() << endl;
    allTransport.push_back(transport);
    //db.close();
}

void Map::deleteStation(Station &station, int i)
{
    QSqlDatabase db = QSqlDatabase::database("SQLMap"); //db.open();
    QSqlQuery query(db);
    query.prepare("DELETE FROM stationTable "
                  "WHERE id = (:id)");
    query.bindValue(":id", station.getId());
    query.exec();
    query.prepare("DELETE FROM routeTable WHERE "
                  "idStation = (:id)");
    query.bindValue(":id", station.getId());
    query.exec();
    allStations.removeAt(i);
    //db.close();
}

void Map::deleteTransport(Transport &transport, int i)
{
    QSqlDatabase db = QSqlDatabase::database("SQLMap"); //db.open();
    QSqlQuery query(db);
    query.prepare("DELETE FROM transportTable "
                  "WHERE id = (:id)");
    query.bindValue(":id", transport.getId());
    query.exec();
    query.prepare("DELETE FROM routeTable WHERE "
                  "idTransport = (:id)");
    query.bindValue(":id", transport.getId());
    query.exec();
    allTransport.removeAt(i);
    //db.close();
}

void Map::deleteStationFromRoute(const Station &station, const Transport &transport, int position)
{
    QSqlDatabase db = QSqlDatabase::database("SQLMap"); //db.open();
    QSqlQuery query(db);
    query.prepare("DELETE FROM routeTable "
                  "WHERE idTransport = (:idTransport) AND "
                  "idStation = (:idStation) AND "
                  "numero = (:numero)");
    query.bindValue(":idTransport", transport.getId());
    query.bindValue(":idStation", station.getId());
    query.bindValue(":numero", position);
    query.exec();
    //db.close();
}

void Map::updateRoute(const QList <Station> &routeList, const Transport &transport)
{
    QSqlDatabase db = QSqlDatabase::database("SQLMap"); //db.open();
    QSqlQuery query(db);
    query.prepare("DELETE FROM routeTable "
                  "WHERE idTransport = (:idTransport)");
    query.bindValue(":idTransport", transport.getId());
    query.exec();
    //db.close();
    for (int i = 0; i < routeList.size(); i++)
    {
        addStationToRoute(routeList[i], transport, i);
    }
}

QList<Station> Map::getAllStations()
{
    QSqlDatabase db = QSqlDatabase::database("SQLMap"); //db.open();
    QList <Station> stationList;
    QSqlQuery query(db);
    query.exec("SELECT * FROM stationTable");
    int columnName = query.record().indexOf("name");
    int columnId = query.record().indexOf("id");
    while (query.next())
    {
        Station station (
                         query.value(columnName).toString(),
                         query.value(columnId).toInt()
                        );
     //   qDebug() << station.getName() << " " << station.getId() << endl ;
        stationList.push_back(station);

    }
    //db.close();
    return stationList;
}

QList<Transport> Map::getAllTransport()
{
    QSqlDatabase db = QSqlDatabase::database("SQLMap"); //db.open();
    QList <Transport> transportList;
    QSqlQuery query(db);
    query.exec("SELECT * FROM transportTable");
    int columnName = query.record().indexOf("name");
    int columnKind = query.record().indexOf("kind");
    int columnInterval = query.record().indexOf("interval");
    int columnId = query.record().indexOf("id");
    while (query.next())
    {
        Transport transport (
                             query.value(columnKind).toString(),
                             query.value(columnName).toString(),
                             query.value(columnInterval).toFloat(),
                             query.value(columnId).toInt()
                             );
      //  qDebug() << transport.getKind() << " " << transport.getName() << " " << transport.getInterval() << " " << transport.getId() << endl ;
        transportList.push_back(transport);
    }
    //db.close();
    return transportList;
}

void Map::addStationToRoute(const Station &station, const Transport &transport, int i)
{
    QSqlDatabase db = QSqlDatabase::database("SQLMap"); //db.open();
    QSqlQuery query(db);

    //adding
    query.prepare("INSERT INTO routeTable (idTransport, idStation, numero) "
                  "VALUES(:idTransport, :idStation, :maxNumero)");
    query.bindValue(":idTransport", transport.getId());
    query.bindValue(":idStation", station.getId());
    query.bindValue(":maxNumero", i);
    query.exec();
    //db.close();
}

QList<Station> Map::getRoute(const Transport &transport)
{
    QSqlDatabase db = QSqlDatabase::database("SQLMap"); //db.open();
    QList <Station> routeList;
    QSqlQuery query(db);
    query.prepare("SELECT * FROM stationTable "
                  "INNER JOIN routeTable ON stationTable.id = routeTable.idStation "
                  "WHERE routeTable.idTransport = (:idTransport) "
                  "ORDER BY routeTable.numero");
    query.bindValue(":idTransport", transport.getId());
    query.exec();
    int columnId = query.record().indexOf("id");
    int columnName = query.record().indexOf("name");
    while (query.next())
    {
        int id = query.value(columnId).toInt();
        QString name = query.value(columnName).toString();
        Station station = Station(name, id);
        routeList.push_back(station);
    }
    //db.close();
    return routeList;
}

QList <Transport> Map::getTransport(const Station &station)
{
    QSqlDatabase db = QSqlDatabase::database("SQLMap"); //db.open();
    QList <Transport> transportList;
    QSqlQuery query(db);
    query.prepare("SELECT * FROM transportTable "
                  "INNER JOIN routeTable ON transportTable.id = routeTable.idTransport "
                  "WHERE routeTable.idStation = (:idStation) "
                  "ORDER BY transportTable.id");
    query.bindValue(":idStation", station.getId());
    query.exec();

    int columnId = query.record().indexOf("id");
    int columnKind = query.record().indexOf("kind");
    int columnName = query.record().indexOf("name");
    int columnInterval = query.record().indexOf("interval");

    while (query.next())
    {
        int id = query.value(columnId).toInt();
        QString kind = query.value(columnKind).toString();
        QString name = query.value(columnName).toString();
        float interval = query.value(columnInterval).toFloat();

      //  qDebug() << name << " " << id << " " << kind << " " << interval << " " << endl;
        Transport transport = Transport(kind, name, interval, id);
        transportList.push_back(transport);
    }
    //db.close();
    return transportList;
}

int Map::getIndex(const Station &station)
{
    int i = 0;
    while ((station.getId() != allStations[i].getId()) && (i < allStations.size())) i++;
    return i;
}

int Map::getIndex(const Transport &transport)
{
    int i = 0;
    while (transport.getId() != allTransport[i].getId() && (i < allTransport.size())) i++;
    return i;
}



