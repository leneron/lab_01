#ifndef DIALOGADDTRANSPORT_H
#define DIALOGADDTRANSPORT_H

#include <QDialog>

namespace Ui {
class DialogAddTransport;
}

class DialogAddTransport : public QDialog
{
    Q_OBJECT

public:
    explicit DialogAddTransport(QWidget *parent = 0);
    ~DialogAddTransport();

    QString getKind() const;
    void setKind(const QString &value);

    QString getName() const;
    void setName(const QString &value);

    float getInterval() const;
    void setInterval(float value);

private slots:
    void on_lineEdit_editingFinished();

    void on_lineEdit_2_editingFinished();

    void on_comboBox_currentIndexChanged(const QString &arg1);

private:
    Ui::DialogAddTransport *ui;
    QString kind;
    QString name;
    float interval;
};

#endif // DIALOGADDTRANSPORT_H
