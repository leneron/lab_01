#include "dialogstation.h"
#include "ui_dialogstation.h"
#include <QList>
#include "Station.h"
#include <QDebug>


DialogStation::DialogStation(QList <Station> &stations, QWidget *parent):
    QDialog(parent),
    ui(new Ui::DialogStation)
{
    ui->setupUi(this);
   // qDebug() << stations.size();
    for (int i = 0; i < stations.size(); i++)
        ui->comboBox->addItem(stations[i].getName());
}

DialogStation::~DialogStation()
{
    delete ui;
}
int DialogStation::getRes() const
{
    return res;
}

void DialogStation::setRes(int value)
{
    res = value;
}


void DialogStation::on_comboBox_currentIndexChanged(int index)
{
    setRes(index);
}
