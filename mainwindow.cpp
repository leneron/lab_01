#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QString>
#include <QFileDialog>
#include <QList>
#include <QListWidget>
#include "Map.h"
#include "dialogstation.h"
#include "dialogroute.h"
#include "dialogaddstation.h"
#include "dialogaddtransport.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    map()
{
    ui->setupUi(this);
    status = UNDEFINED;

    ui->toolAdd->setEnabled(false);
    ui->toolDelete->setEnabled(false);
    ui->toolMoveUp->setEnabled(false);
    ui->toolMoveDown->setEnabled(false);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionNew_triggered()
{
    ui->toolAdd->setEnabled(false);
    ui->toolDelete->setEnabled(false);
    ui->toolMoveUp->setEnabled(false);
    ui->toolMoveDown->setEnabled(false);

    ui->listWidget->clear();
    ui->tooltip->setText("");
    status = UNDEFINED;

    QString filename = QFileDialog::getSaveFileName(this, tr("New Document"),
                                                    QDir::currentPath(),
                                                    tr("Database *.sqlite"));
    QString extension = ".sqlite";
    int i = filename.lastIndexOf(extension);
    if ((i == -1) || (i != (filename.length() - extension.length()))) filename += extension;
    QFile file(filename);
    file.open(QIODevice::WriteOnly);
    file.close();

    map = Map(filename);

    map.createNewTables();
}

void MainWindow::on_actionOpen_triggered()
{
    ui->toolAdd->setEnabled(false);
    ui->toolDelete->setEnabled(false);
    ui->toolMoveUp->setEnabled(false);
    ui->toolMoveDown->setEnabled(false);

    ui->listWidget->clear();
    ui->tooltip->setText("");
    status = UNDEFINED;

    QString filename = QFileDialog::getOpenFileName(this, tr("Open Document"),
                                                    QDir::currentPath(),
                                                    tr("Database *.sqlite"));
  //  qDebug() << filename;
    map = Map(filename);

}

void MainWindow::on_actionAll_stations_triggered()
{
    ui->toolAdd->setEnabled(true);
    ui->toolDelete->setEnabled(true);
    ui->toolMoveUp->setEnabled(false);
    ui->toolMoveDown->setEnabled(false);

    ui->listWidget->clear();
    ui->tooltip->setText("List of all stations");
    map.allStations = map.getAllStations();

    if (map.allStations.size() == 0) ui->listWidget->addItem("ERROR: Stations don't exist");
    else for (int i = 0; i < map.allStations.size(); i++)
    {
        ui->listWidget->addItem("name: " + map.allStations[i].getName());
    }
    status = ALLSTATIONS;
}


void MainWindow::on_actionAll_transport_triggered()
{
    ui->toolAdd->setEnabled(true);
    ui->toolDelete->setEnabled(true);
    ui->toolMoveUp->setEnabled(false);
    ui->toolMoveDown->setEnabled(false);

    ui->listWidget->clear();
    ui->tooltip->setText("List of all transport");
    map.allTransport = map.getAllTransport();

    if (map.allTransport.size() == 0) ui->listWidget->addItem("ERROR: Transport doesn't exist");
    else for (int i = 0; i < map.allTransport.size(); i++)
    {
        QString interval = QString::number(map.allTransport[i].getInterval());
        ui->listWidget->addItem("name: " + map.allTransport[i].getName() +
                                "\ninterval: " + interval + " min" +
                                "\nkind: " + map.allTransport[i].getKind());
    }
    status = ALLTRANSPORT;
}

void MainWindow::on_actionAll_transport_on_station_triggered()
{
    ui->toolAdd->setEnabled(false);
    ui->toolDelete->setEnabled(true);
    ui->toolMoveUp->setEnabled(false);
    ui->toolMoveDown->setEnabled(false);

    status = TRANSPORTONSTATION;
    ui->listWidget->clear();
    ui->tooltip->setText("List of all transport stopping on the station ..." );
    map.allStations = map.getAllStations();
    if (map.allStations.size() == 0) ui->listWidget->addItem("ERROR: Stations don't exist");
    else
    {
        DialogStation dialog(map.allStations);
        dialog.setWindowTitle("Show all transport on the station");
        if (dialog.exec() == DialogStation::Accepted)
        {
           // qDebug() << dialog.getRes();
            int index = dialog.getRes();
            QList <Transport> tempTransport = map.getTransport(map.allStations[index]);
            ui->tooltip->setText("List of all transport stopping on the station "
                                 + map.allStations[index].getName());
            if (tempTransport.size() == 0) ui->listWidget->addItem("ERROR: Transport doesn't exist");
            else for (int i = 0; i < tempTransport.size(); i++)
            {
                QString interval = QString::number(tempTransport[i].getInterval());
                ui->listWidget->addItem("name: " + tempTransport[i].getName() +
                                        "\ninterval: " + interval + " min" +
                                        "\nkind: " + tempTransport[i].getKind());
            }
        }
    }
}

void MainWindow::on_actionRoute_of_the_transport_triggered()
{
    ui->toolAdd->setEnabled(true);
    ui->toolDelete->setEnabled(true);
    ui->toolMoveUp->setEnabled(true);
    ui->toolMoveDown->setEnabled(true);
    status = ROUTE;
    ui->listWidget->clear();
    ui->tooltip->setText("List of the route of the transport ..." );
    map.allTransport = map.getAllTransport();
    map.allStations = map.getAllStations();
    if (map.allTransport.size() == 0) ui->listWidget->addItem("ERROR: Transport doesn't exist");
    else
    {
        DialogRoute dialog(map.allTransport);
        dialog.setWindowTitle("Show the route");
        if (dialog.exec() == DialogRoute::Accepted)
        {
            int index = dialog.getRes();
            QList <Station> tempStation = map.getRoute(map.allTransport[index]);
            ui->tooltip->setText("List of the route of the transport "
                                 + map.allTransport[index].getName());
            map.currTransport = index;
            if (tempStation.size() == 0) ui->listWidget->addItem("ERROR: Stations don't exist");
            else for (int i = 0; i < tempStation.size(); i++)
            {
                ui->listWidget->addItem("name: " + tempStation[i].getName());
            }
        }
    }
}

void MainWindow::on_toolAdd_clicked()
{
    switch (status)
    {
     case 1:{DialogAddStation dialog;
            dialog.setWindowTitle("Add the station");
            if (dialog.exec() == DialogAddStation::Accepted)
            {
                QString name = dialog.getRes();
                if (map.allStations.size() == 0) ui->listWidget->clear();
                ui->listWidget->addItem("name: " + name);
                Station station = Station(name);
                map.addStation(station);
            }} break;
    case 2:{DialogAddTransport dialog;
           dialog.setWindowTitle("Add transport");
           if (dialog.exec() == DialogAddTransport::Accepted)
           {
               QString name = dialog.getName();
               float interval = dialog.getInterval();
               QString kind = dialog.getKind();
               if (map.allTransport.size() == 0) ui->listWidget->clear();
               ui->listWidget->addItem("name: " + name +
                                       "\ninterval: " + QString::number(interval) + " min" +
                                       "\nkind: " + kind);
               Transport transport = Transport(kind, name, interval);
               map.addTransport(transport);
           }} break;
    case 4:{DialogStation dialog(map.allStations);
           dialog.setWindowTitle("Add station to route: select station");
           if (dialog.exec() == DialogStation::Accepted)
           {
               int index = dialog.getRes();
               QList <Station> temp = map.getRoute(map.allTransport[map.currTransport]);
               int i = temp.size();
               if (i == 0) ui ->listWidget->clear();
               map.addStationToRoute(map.allStations[index], map.allTransport[map.currTransport], i);
               ui->listWidget->addItem("name: " + map.allStations[index].getName());
           }
           } break;
    }
}

void MainWindow::on_toolDelete_clicked()
{
    if (status > 0)
    {
        int index = ui->listWidget->row(ui->listWidget->currentItem());
        if (index >= 0)
        {
            switch (status)
            {
             case 1: map.deleteStation(map.allStations[index], index); break;
             case 2: map.deleteTransport(map.allTransport[index], index); break;
             case 3:{
                      QList <Transport> temp = map.getTransport(map.allStations[map.currStation]);
                      int i = map.getIndex(temp[index]);
                      map.deleteStationFromRoute(map.allStations[map.currStation], map.allTransport[i], index);
                    } break;
             case 4:{
                      QList <Station> temp = map.getRoute(map.allTransport[map.currTransport]);
                      int i = map.getIndex(temp[index]);
                      map.deleteStationFromRoute(map.allStations[i], map.allTransport[map.currTransport], index);
                    } break;
            }
            ui->listWidget->takeItem(index);
        }
    }
}

void MainWindow::on_toolMoveUp_clicked()
{
    if ((status == 4) && (ui->listWidget->row(ui->listWidget->currentItem()) > 0))
    {
        int index = ui->listWidget->row(ui->listWidget->currentItem());
        QList <Station> temp = map.getRoute(map.allTransport[map.currTransport]);
        int length = ui->listWidget->count();
        QListWidgetItem *pItem = ui->listWidget->takeItem(index);
        ui->listWidget->insertItem(index-1, pItem);

        std::swap(temp[index], temp[index-1]);
        ui->listWidget->setCurrentRow(index-1);
        map.updateRoute(temp, map.allTransport[map.currTransport]);
    }
}

void MainWindow::on_toolMoveDown_clicked()
{
    if ((status == 4) && (ui->listWidget->row(ui->listWidget->currentItem()) < ui->listWidget->count()))
    {
        int index = ui->listWidget->row(ui->listWidget->currentItem());
        QList <Station> temp = map.getRoute(map.allTransport[map.currTransport]);
        QListWidgetItem *pItem = ui->listWidget->takeItem(index);
        int length = ui->listWidget->count();
        ui->listWidget->insertItem(index+1, pItem);

        std::swap(temp[index], temp[index+1]);
        ui->listWidget->setCurrentRow(index+1);
        map.updateRoute(temp, map.allTransport[map.currTransport]);
    }
}

