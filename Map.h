#ifndef MAP_H
#define MAP_H
#include <QString>
#include <QList>
#include <QSqlQuery>
#include <QPoint>
#include "Station.h"
#include "Transport.h"

class Map
{
public:
    Map(const QString &filename);
    Map();
    ~Map();

    void createNewTables();

    void addStation(Station &station);
    void addTransport(Transport &transport);

    void deleteStation(Station &station, int i);
    void deleteTransport(Transport &transport, int i);
    void deleteStationFromRoute(const Station &station, const Transport &transport, int position);


    QList <Station> getAllStations();
    QList <Transport> getAllTransport();

    void addStationToRoute(const Station &station, const Transport &transport, int i);
    void updateRoute(const QList <Station> &routeList, const Transport &transport);

    QList <Station> getRoute(const Transport& transport);
    QList <Transport> getTransport(const Station& station);

    QList <Station> allStations;
    QList <Transport> allTransport;
    int currStation;
    int currTransport;

    int getIndex(const Station &station);
    int getIndex(const Transport &transport);
//private:
    //QSqlDatabase db;

};

#endif // MAP_H
