#include "Station.h"
#include <QDebug>

Station::Station(const QString &valueName = "")
{
    name = valueName;
}

Station::Station(const QString &valueName, int valueId)//, const QPoint &valueP)
{
    name = valueName;
    setId(valueId);
}

Station::~Station()
{

}

QString Station::getName() const
{
    return name;
}

void Station::setName(const QString &value)
{
    name = value;
}

int Station::getId() const
{
    return id;
}

void Station::setId(int value)
{
    id = value;
}

